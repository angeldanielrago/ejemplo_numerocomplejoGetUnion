/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;

/**
 * Clase representa una persona con su fecha de nacimiento
 *
 * @author Daniel
 * @author Bridenit
 */
public class Persona_Excepcion implements Comparable {

    private String nombresCompletos;
    //Su fecha de nacimiento
    private short dia, mes, agno, hora, min, seg;

    /**
     * Se crea un constructor Persona vacío
     */
    public Persona_Excepcion() {
    }

    /**
     * Se crea un objeto Persona
     *
     * @param nombresCompletos Un String que representa los nombres de la
     * persona
     * @param dia Un Short que representa día
     * @param mes Un Short que representa mes
     * @param agno Un Short que representa agno
     * @param hora Un Short que representa hora
     * @param min Un Short que representa min
     * @param seg Un Short que representa seg
     */
    public Persona_Excepcion(String nombresCompletos, short dia, short mes, short agno, short hora, short min, short seg) throws Exception {
        this.nombresCompletos = nombresCompletos;
        this.dia = dia;
        this.mes = mes;
        this.agno = agno;
        this.hora = hora;
        this.min = min;
        this.seg = seg;
        this.validarHora();
        this.validarFecha();

    }

    private void validarHora() throws Exception {
        //transformar las entradas en salidas
        boolean horaValida = (this.hora < 24) & (this.min < 60) & (this.seg < 60);
        if (!horaValida) {
            throw new Exception(" La hora ingresada no es valida:" + this.hora + ":" + this.min + ":" + this.seg);
        }
    }

    private void validarFecha() throws Exception {

        boolean diaV, mesV, agnoV, fechaV;

        boolean bisiesto = this.agno % 4 == 0 || this.agno % 100 == 0 || this.agno % 400 == 0;
        diaV = this.dia >= 1 && this.dia <= 31;
        mesV = this.mes >= 1 && this.mes <= 12;
        agnoV = this.agno >= 1;
        fechaV = diaV && agnoV && (((this.mes == 4 && dia <= 30 || this.mes == 6 && dia <= 30 || this.mes == 9 && dia <= 30 || this.mes == 11 && dia <= 30) || (this.mes == 2 && !bisiesto && dia <= 28) || (this.mes == 2 && bisiesto && dia <= 29))
                || (this.mes == 1 || this.mes == 3 || this.mes == 5 || this.mes == 7 || this.mes == 8 || this.mes == 10 || this.mes == 12 && dia <= 31));
        if (!fechaV) {
            throw new Exception("La fecha ingresada no es valida:" + this.dia + "/" + this.mes + "/" + this.agno);
        }
    }
    //Requisito 2

    /**
     * returna el valor Nombres de Persona
     *
     * @return un String con su respectivo nombre
     */
    public String getNombresCompletos() {
        return nombresCompletos;
    }

    /**
     * Actualiza el valor Nombres de Persona
     *
     * @param nombresCompletos nuevo valor para Nombre.
     */
    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }

    public short getDia() {
        return dia;
    }

    public void setDia(short dia) {
        this.dia = dia;
    }

    public short getMes() {
        return mes;
    }

    public void setMes(short mes) {
        this.mes = mes;
    }

    public short getAgno() {
        return agno;
    }

    public void setAgno(short agno) {
        this.agno = agno;
    }

    public short getHora() {
        return hora;
    }

    public void setHora(short hora) {
        this.hora = hora;
    }

    public short getMin() {
        return min;
    }

    public void setMin(short min) {
        this.min = min;
    }

    public short getSeg() {
        return seg;
    }

    public void setSeg(short seg) {
        this.seg = seg;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    /**
     *
     * @param obj Persona a ser comparado
     * @return el resultado de la comparación.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        //MiddlerCasting--> Convierte el Object a su clase respectiva
        //Final para evitar que cambie sus atributos -->Kte
        final Persona_Excepcion other = (Persona_Excepcion) obj;

        if (this.dia != other.dia) {
            return false;
        }
        if (this.mes != other.mes) {
            return false;
        }
        if (this.agno != other.agno) {
            return false;
        }
        if (this.hora != other.hora) {
            return false;
        }
        if (this.min != other.min) {
            return false;
        }
        if (this.seg != other.seg) {
            return false;
        }
        if (!Objects.equals(this.nombresCompletos, other.nombresCompletos)) {
            return false;
        }
        return true;
    }

    /**
     * Por lo general es una resta y su resultado es: 0 == si this ==
     * segundo_objeto mayor 0 cuando this es mayor al segundo_objeto, menor 0
     * cuando this es menor al segundo_objeto,
     *
     * @param obj el objeto a comparar
     * @return un entero con la comparación(factor)
     */
    @Override
    public int compareTo(Object obj) {

        if (this == obj) {
            return 0;
        }
        if (obj == null) { // THIS==NULL-- > SOBRA
            throw new RuntimeException("No se puede comparar porque el segundo objeto es null");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("Objetos no son de la misma Clase");
        }
        final Persona_Excepcion other = (Persona_Excepcion) obj;
        /**
         * Se compara el año, si es igual pasamos a comparar el mes, si año y
         * mes son iguales se compara el dia, hora, minuto y segundo. Todo esto
         * se hizo a travez de convertir los dias, horas y minuto en segundos.
         */
        int comparador_agnos = this.agno - other.agno;
        if (comparador_agnos == 0) {
            int comparacionMes = this.mes - other.mes;
            if (comparacionMes == 0) {
                int segundosTotal1 = this.dia * 86400 + this.hora * 3600 + this.min * 60 + this.seg;
                int segundosTotal2 = other.dia * 86400 + other.hora * 3600 + other.min * 60 + other.seg;
                int restaSegundos = segundosTotal1 - segundosTotal2;
                return restaSegundos;
            } else {
                return comparacionMes;
            }
        }

        return comparador_agnos;

    }

    /**
     *
     * @return la forma en que se va a mostrar en pantalla.
     */
    @Override
    public String toString() {
        return "" + nombresCompletos + ", Nació=" + dia + "/" + mes + "/" + agno + ", A las " + hora + ":" + min + ":" + seg;
    }

}
