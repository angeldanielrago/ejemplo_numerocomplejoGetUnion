/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Está clase representa un número complejo z=a+bi
 * con b mayor 0 y menor 100
 *
 * @author madarme
 */
public class NumeroComplejo_Excepcion implements Comparable {

    private float parteReal;
    private float imaginario;

    /**
     * Crea un número complejo vacío
     */
    public NumeroComplejo_Excepcion() {
    }

    /**
     * Crea un número complejo
     *
     * @param parteReal un float que representa su componente real
     * @param imaginario un float que representa su parte imaginaria
     */
    public NumeroComplejo_Excepcion(float parteReal, float imaginario) throws Exception {
        this.parteReal = parteReal;
        this.imaginario = imaginario;
        
        //validar lógica del negocio
        this.validar_0_100();
        
    }

    
    private void validar_0_100() throws Exception {

        boolean validar = (this.imaginario >= 0 && this.imaginario <= 100);
        if (!validar) {
            throw new Exception("Dato fuera de rango:" + this.imaginario);
        }
        
    }
    
    
    
    /**
     * Retorna el valor del real del número complejo
     *
     * @return un float con su parte real
     */
    public float getParteReal() {
        return parteReal;
    }

    /**
     * Actualiza el valor del real del número complejo
     *
     * @param parteReal nuevo valor para su parte real
     */
    public void setParteReal(float parteReal) {
        this.parteReal = parteReal;
    }

    public float getImaginario() {
        return imaginario;
    }

    public void setImaginario(float imaginario) {
        this.imaginario = imaginario;
    }

    @Override
    public String toString() {
        return this.parteReal + "+√" + this.imaginario + " i";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Float.floatToIntBits(this.parteReal);
        hash = 89 * hash + Float.floatToIntBits(this.imaginario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) { // THIS==NULL-- > SOBRA
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        //MiddlerCasting--> Convierte el Object a su clase respectiva
        //Final para evitar que cambie sus atributos -->Kte
        final NumeroComplejo_Excepcion other = (NumeroComplejo_Excepcion) obj;

        if (Float.floatToIntBits(this.parteReal) != Float.floatToIntBits(other.parteReal)) {
            return false;
        }
        if (Float.floatToIntBits(this.imaginario) != Float.floatToIntBits(other.imaginario)) {
            return false;
        }
        return true;
    }

    /**
     * Por lo general es una resta y su resultado es: 0 == si this ==
     * segundo_objeto mayor 0 cuando this es mayor al segundo_objeto, menor 0
     * cuando this es menor al segundo_objeto,
     *
     * @param obj el objeto a comparar
     * @return un entero con la comparación(factor)
     */
    @Override
    public int compareTo(Object obj) {

        if (this == obj) {
            return 0;
        }
        if (obj == null) { // THIS==NULL-- > SOBRA
            throw new RuntimeException("No se puede comparar por que el segundo objeto es null");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("Objetos no son de la misma Clase");
        }
        final NumeroComplejo_Excepcion other = (NumeroComplejo_Excepcion) obj;
        /**
         * ¿CUANDO UN NUMERO COMPLEJO ES MAYO QUE OTRO? num1=5+9i y num2=7+9i
         * --> num1 menor a num2 num3=5+19i y num4=5000+9i --> num3 mayor a num4
         *
         * NUESTRO FACTOR DE COMPARACIÓN ES SU PARTE IMAGINARIA
         */
        float comparador_Real = this.parteReal - other.parteReal;
        float comparador_Imaginaria = this.imaginario - other.imaginario;

        if (comparador_Imaginaria == 0) {
            return (int) comparador_Real;
        }

        return (int) comparador_Imaginaria;

    }

}
