/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.util.Arrays;

/**
 * Nuestra primera estructura : Vector--> ListaNumeros
 *
 * @author madarme
 */
public class ListaNumeros implements Comparable {

    private float[] numeros;

    public ListaNumeros() {
    }

    public ListaNumeros(int cant) {
        if (cant <= 0) {
            throw new RuntimeException("No se puede crear el vector");
        }

        this.numeros = new float[cant];

    }

    /**
     * Adiciona un número en la posición i
     *
     * @param i indice donde se va a ingresar el dato
     * @param numeroNuevo el dato a ingresar
     */
    public void adicionar(int i, float numeroNuevo) {
        this.validar(i);
        this.numeros[i] = numeroNuevo;
    }

    public float getElemento(int i) {

        this.validar(i);
        return this.numeros[i];
    }

    private void validar(int i) {
        if (i < 0 || i >= this.numeros.length) {
            throw new RuntimeException("Índice fuera de rango:" + i);
        }
    }

    /*
        ES REDUNDANTE
    **/
    public void actualizar(int i, float numeroNuevo) {
        this.adicionar(i, numeroNuevo);
    }

    public float[] getNumeros() {
        return numeros;
    }

    public void setNumeros(float[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {
        String msg = "";
        /*
            Cuando sólo son recorridos, usamos el foreach
            for(T elemento:coleccion)
                hacer algo con elemento;
         */

        for (float dato : this.numeros) {
            msg += dato + "\t";
        }
        return msg;
    }

    public int length() {
        return this.numeros.length;
    }

    /**
     * Elimina la posición i del vector y REDIMENSIONA EL VECTOR
     *
     * @param i la posición a eliminar
     */
    public void eliminar(int i) {

    }

    /**
     * Ordena el vector por el método de la burbuja
     */
    public void ordenar_Burbuja() {

    }

    /**
     * Ordena el vector por el método de selección
     */
    public void ordenar_Seleccion() {

    }

    /**
     * Retorna una lista de numeros con la unión de conjuntos de lista original
     * con lista dos.
     *
     * Se debe validar que la listanueva contenga UNICAMENTE LA CANTIDAD DE
     * CELDAS REQUERIDAS
     *
     * Ejemplo: this={3,4,5,6} y dos={3,4} ListaNueva = {3,4,5,6}
     *
     * @param dos una lista de numeros
     * @return una nueva lista con la unión de conjuntos
     */
    private void pasar(float origen[], float destino[], int posInicial) {
        for (int i = 0; i < origen.length; i++) {

            destino[posInicial] = origen[i];
            posInicial++;
        }

    }

    public ListaNumeros getUnion(ListaNumeros dos) {

        ListaNumeros union = new ListaNumeros(numeros.length + dos.length());

        pasar(this.numeros, union.numeros, 0);
        pasar(dos.numeros, union.numeros, this.numeros.length);

        union = buscarRepetidos(union);
        return union;
    }

    public ListaNumeros eliminarRepetidos(ListaNumeros union, int indiceRepetido) {

        ListaNumeros nuevaLista = new ListaNumeros(union.length() - 1);

        for (int i = 0, j = 0; i < union.length(); i++) {

            if (i != indiceRepetido) {
                nuevaLista.adicionar(j++, union.getElemento(i));

            }

        }

        return nuevaLista;
    }

    /*

    l=<1,2,3,1,2,3>
    J=0, I=J+1
    POR PROPIEDAD DE CONJUNTOS LOS ELEMENTOS NO SE REPITEN
     */
    public ListaNumeros buscarRepetidos(ListaNumeros union) {

        for (int j = 0; j < union.length(); j++) {

            //i=J+1
            for (int i = j + 1; i < union.length(); i++) {

                if (union.getElemento(j) == union.getElemento(i)) {

                    union = eliminarRepetidos(union, i);

                }
            }
        }
        return union;

    }

    /**
     * Retorna una lista de numeros con la INTERSECCION de conjuntos de lista
     * original con lista dos.
     *
     * Se debe validar que la listanueva contenga UNICAMENTE LA CANTIDAD DE
     * CELDAS REQUERIDAS
     *
     * Ejemplo: this={3,4,5,6} y dos={3,4} ListaNueva = {3,4}
     *
     * @param dos una lista de numeros
     * @return una nueva lista con la unión de conjuntos
     */
    public ListaNumeros getInterseccion(ListaNumeros dos) {

        ListaNumeros aux1, aux2;

        if (this.numeros.length <= dos.length()) {
            aux1 = this;
            aux2 = dos;

        } else {
            aux1 = dos;
            aux2 = this;
        }
        ListaNumeros temporal = new ListaNumeros(aux1.length());
        int con = 0;
        int aux = 0;
        for (int i = 0; i < aux1.length(); i++) {
            if (existe(aux2, aux1.getElemento(i))) {
                temporal.adicionar(aux, aux1.getElemento(i));
                aux++;
                con++;
            }
        }
        ListaNumeros interseccion;
        interseccion = new ListaNumeros(con);
        for (int i = 0; i < interseccion.length(); i++) {
            interseccion.adicionar(i, temporal.getElemento(i));
        }
        return interseccion;
    }

    private static boolean existe(ListaNumeros dos, float aux) {
        boolean existe = false;
        for (int i = 0; i < dos.length(); i++) {
            if (dos.getElemento(i) == aux) {
                return true;
            }
        }
        return existe;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.hashCode(this.numeros);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaNumeros other = (ListaNumeros) obj;

        if (this.numeros.length != other.numeros.length) {
            return false;
        }

        for (int i = 0; i < this.numeros.length; i++) {
            if (this.numeros[i] != other.numeros[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Una lista es mayor a otra si y solo si: 1. Tienen el mismo tamaño 2.
     * Todos los elementos de la lista1 son mayores a los de la lista 2 en orden
     * ( suponga que la lista 1 y 2 se encuentra ordenada)
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
