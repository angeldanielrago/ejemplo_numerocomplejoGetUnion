/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.NumeroComplejo;

/**
 *
 * @author madar
 */
public class TestNumeroComplejo {

    public static void main(String[] args) {
        //num1=5+9i y num2=7+9i
        NumeroComplejo num1 = new NumeroComplejo(5, 9);
        NumeroComplejo num2 = new NumeroComplejo(7, 9);
        //Probando toString():
        System.out.println(num1.toString());
        System.out.println(num2.toString());
        //Probando equals:
        if (num1.equals(num2)) {
            System.out.println(num1.toString() + "\t Es igual a :) " + num2.toString());
        } else {
            System.out.println(num1.toString() + "\t No Son iguales a :( " + num2.toString());
        }

        System.out.println("----------- Probando compareTo ---------------------");
        int c = num1.compareTo(num2);
        if (c == 0) {
            System.out.println(num1.toString() + "\t Es igual a :) " + num2.toString());
        } else if (c < 0) {
            System.out.println(num1.toString() + "\t Es MENOR a :) " + num2.toString());
        } else {
            System.out.println(num1.toString() + "\t Es MAYOR a :) " + num2.toString());
        }

        System.out.println("----------- Probando compareTo con errores---------------------");
        try {
            c = num1.compareTo("PEPE");
            System.out.println(c);
        } catch (Exception e) {
            System.err.println("UD tiene un error:" + e.getMessage());
        }

    }
}
