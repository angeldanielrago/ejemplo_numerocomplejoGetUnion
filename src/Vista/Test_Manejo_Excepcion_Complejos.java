/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.NumeroComplejo;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madarme
 */
public class Test_Manejo_Excepcion_Complejos {

    public static void main(String[] args) {
        NumeroComplejo x = new NumeroComplejo(leerFloat("Digite parte Real:"), leerImaginario());
        System.out.println(x.toString());
        /*
        NOS PIDEN CREAR NUMEROS COMPLEJOS CUYA 
        PARTE IMAGINARIA SEA >0 Y MENOR 100
        
         */
    }

    private static float leerFloat(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextFloat();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerFloat(msg);
        }

    }

    /**
     * EXCEPCIÓN OBLIGATORIA EN EL CÓDIGO Método sólo retorna datos de 0 a 100
     * para la parte imaginaria del número complejo
     *
     * @param msg El título para digitar los datos
     * @return un float de 0 a 100
     */
    private static float leerImaginario_0_100() throws Exception {

        float dato = leerFloat("Digite Imaginario");
        boolean validar = (dato >= 0 && dato <= 100);
        if (!validar) {
            throw new Exception("Dato fuera de rango:" + dato);
        }
        return dato;
    }

    private static float leerImaginario() {
        try {
            return leerImaginario_0_100();
        } catch (Exception ex) {
            System.out.println("Error:" + ex.getMessage());
            return leerImaginario();
        }
    }
}
