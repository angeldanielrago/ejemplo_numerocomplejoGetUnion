/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;

/**
 *
 * @author Daniel R
 * @author Dayana
 */
public class TestPersona {
    public static void main2(String[] args) {
        Persona p1 = new Persona("Daniel", (short) 30, (short) 9, (short) 2000, (short) 23, (short) 30, (short) 22);
        Persona p2 = new Persona("Dayana", (short) 30, (short) 9, (short) 2000, (short) 23, (short) 31, (short) 22);
        Persona p3 = new Persona("Andres", (short) 20, (short) 5, (short) 2000, (short) 4, (short) 40, (short) 15);
        Persona p4 = new Persona("Diego", (short) 12, (short) 9, (short) 2002, (short) 7, (short) 30, (short) 45);
         System.out.println("----------- Probando compareTo con errores---------------------");
        try {
            int d;

            d = p1.compareTo(null);

        } catch (Exception e) {
            System.err.println("UD tiene un error:" + e.getMessage());
        }
    
    
    }
    public static void main(String[] args) {

        Persona p1 = new Persona("Daniel", (short) 30, (short) 9, (short) 2000, (short) 23, (short) 30, (short) 22);
        Persona p2 = new Persona("Dayana", (short) 30, (short) 9, (short) 2000, (short) 23, (short) 31, (short) 22);
        Persona p3 = new Persona("Andres", (short) 20, (short) 5, (short) 2000, (short) 4, (short) 40, (short) 15);
        Persona p4 = new Persona("Diego", (short) 12, (short) 9, (short) 2002, (short) 7, (short) 30, (short) 45);

        //Probando toString():
        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p3.toString());
        System.out.println(p4.toString());

       
        /**
         * Se crea arreglo y se insertan los objetos personas
         */
        Persona personas[] = new Persona[4];
        personas[0] = p1;
        personas[1] = p2;
        personas[2] = p3;
        personas[3] = p4;
        /**
         * Se crea un ciclo el cual compara cada una de las personas 1->2, 1->3,
         * 1->4, 2->3 , 2->4, 3->4
         */
        for (int j = 0; j < personas.length; j++) {
            int i = j + 1;

            while (i < personas.length) {

                System.out.println("----------- Probando Equals ---------------------");
                //Probando equals:
                if (personas[j].equals(personas[i])) {
                    System.out.println(personas[j].toString() + "\t Son la misma persona :) " + personas[i].toString());
                } else {
                    System.out.println(personas[j].toString() + "\t Son personas distintas :( " + personas[i].toString());
                }

                System.out.println("----------- Probando compareTo ---------------------");

                int c = personas[j].compareTo(personas[i]);
                if (c == 0) {
                    System.out.println(personas[j].toString() + "\t Tiene la misma edad a :) " + personas[i].toString());
                } else if (c > 0) {
                    System.out.println(personas[j].toString() + "\t Es MENOR a :) " + personas[i].toString());
                } else {
                    System.out.println(personas[j].toString() + "\t Es MAYOR a :) " + personas[i].toString());
                }
                System.out.println("-------------------------------------------------->");
                i++;
            }

        }

    }
}
