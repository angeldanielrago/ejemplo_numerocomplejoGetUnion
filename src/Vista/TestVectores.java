/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.ListaNumeros;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class TestVectores {

    public static void main(String[] args) {

        ListaNumeros lista = new ListaNumeros(leerEntero("Digite cantidad de elementos de la lista 1: "));
        ListaNumeros lista2 = new ListaNumeros(leerEntero("Digite cantidad de elementos de la lista 2: "));
        /**
         * 2
         * Como vamos a realizar un proceso de almacenamiento usamos el for
         * convencional
         */
        for (int i = 0; i < lista.length(); i++) {
            lista.adicionar(i, leerFloat("Digite dato [" + i + "]:"));
        }
        System.out.println("Su lista es:" + lista.toString());

        for (int i = 0; i < lista2.length(); i++) {
            lista2.adicionar(i, leerFloat("Digite dato [" + i + "]:"));

        }
        System.out.println("Su lista 2 es: " + lista2.toString());

        System.out.println("La unión entre las dos listas es: " + lista.getUnion(lista2));
    } 

    private static float leerFloat(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextFloat();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerFloat(msg);
        }

    }

    private static int leerEntero(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerEntero(msg);
        }

    }

}
