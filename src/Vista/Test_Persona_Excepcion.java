/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;
import Modelo.Persona_Excepcion;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class Test_Persona_Excepcion {

    public static void main(String[] args) {
        Persona_Excepcion x = crearPersona();
        System.out.println(x.toString());

    }

    private static Persona_Excepcion crearPersona() {
        Persona_Excepcion x;
        System.out.println("-------------------- Creando Personas ----------------\n");
        String nombre;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nombre: ");
        nombre = sc.nextLine();
        try {
            x = new Persona_Excepcion(nombre, leerShort("Digite el dia: "), leerShort("Digite el mes: "), leerShort("Digite el año: "), leerShort("Digite la hora: "), leerShort("Digite el minuto: "), leerShort("Digite el segundo: "));
            return x;
        } catch (Exception ex) {
            System.err.println("Error:" + ex.getMessage());
            return crearPersona();
        }

    }

    private static short leerShort(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextShort();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Short");
            return leerShort(msg);
        }

    }
}


 
